{
	"name": "The Fixes",
	"description": "Lots of fixes for base game issues",
	"author": "andole; Dom",
	"version": "30.17",
	"blt_version" : 2.0,
	"priority" : -10,
	"color" : "0 0.6 0.286",
	"image" : "logo.png",
	"contact": "\n http://steamcommunity.com/id/andole/",
	"supermod_definition" : "fixes.xml",

	"updates" : [
		{
			"identifier" : "pd2_the_fixes_for_chinese",
			"host": {
				"meta": "https://gitlab.com/LeftOwlRight/the-fixes/-/raw/main/meta.json",
				"download": "https://gitlab.com/LeftOwlRight/the-fixes/-/archive/main/the-fixes-main.zip",
				"patchnotes": "https://modworkshop.net/mydownloads.php?action=view_down&did=23732#changelog"
			}
		}
	]
}