function TimerGui:post_event(event)
	if not event then
		return
	end

	local sound_event = event

	if event == self._start_event or event == self._resume_event or event == self._done_event then
		if self._skill == 3 then
			sound_event = sound_event .. "_aced"
		elseif self._skill == 2 then
			sound_event = sound_event .. "_basic"
		end
	--[[elseif event == self._jam_event and self._skill == 3 and managers.groupai:state():whisper_mode() then
		self._unit:sound_source():stop()

		return]] -- Why mute the drill ?
	end

	local clbk, cookie = nil, nil
	local flags = {}

	if event == self._done_event then
		self._is_playing_done_event = true
		clbk = callback(self, self, "on_done_event_ended")

		table.insert(flags, "end_of_event")
	end

	self._unit:sound_source():post_event(sound_event, clbk, cookie, unpack(flags))
end