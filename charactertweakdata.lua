local original = CharacterTweakData._init_team_ai
function CharacterTweakData:_init_team_ai(...)
    original(self, ...)
	local team_ai_tweak_names = {
		"russian",
		"german",
		"spanish",
		"american",
		"jowi",
		"old_hoxton",
		"female_1",
		"dragan",
		"jacket",
		"bonnie",
		"sokol",
		"dragon",
		"bodhi",
		"jimmy",
		"sydney",
		"wild",
		"chico",
		"max",
		"joy",
		"myh",
		"ecp_female",
		"ecp_male"
	}
    for _, tweak_name in ipairs(team_ai_tweak_names) do
        local tweak = self[tweak_name]
        if tweak and tweak.weapon and tweak.weapon.weapons_of_choice then
            tweak.weapon.weapons_of_choice.secondary = Idstring("units/payday2/weapons/wpn_npc_beretta92/wpn_npc_beretta92")
        end
    end
end