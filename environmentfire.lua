function EnvironmentFire:load(data)
	local state = data.EnvironmentFire
	self._burn_duration = state.burn_duration
	self._user_unit = state.user_unit
	self._weapon_unit = state.weapon_unit
	self._burn_duration = state.burn_duration
	self._burn_tick_counter = state.burn_tick_counter
	self._burn_tick_period = state.burn_tick_period
	self._range = state.range
	self._curve_pow = state.curve_pow
	self._damage = state.damage
	self._player_damage = state.player_damage
	self._dot_data = state.dot_data
	self._fire_alert_radius = state.fire_alert_radius
	self._no_fire_alert = state.no_fire_alert
	self._is_molotov = state.is_molotov
	self._hexes = state.hexes or 6
	local data = state.data
	local normal = state.normal
	local added_time = state.added_time
	local range_multiplier = state.range_multiplier

	self:on_spawn(data, normal, self._user_unit, self._weapon_unit, added_time, range_multiplier)
end